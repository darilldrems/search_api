from flask import Flask, session, flash, request, jsonify, redirect
from functools import wraps
import os, sys
sys.path.insert(0, os.path.abspath(".."))

from myconfig import app_config

from libraries.embedly import get_terms_from_page
from libraries.solrconnector import SolrConnector
from libraries.filesystemcache import FileSystemCache
from libraries.similarity_ranking import rank_by_similarity, rank_by_terms_scores

import random

app = Flask(__name__)

#this is used for authenticating the requesting client
def token_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		# print "token is: ", request.args.get("token")
		if request.args.get("token") == app_config.get("api_key"):
			return f(*args, **kwargs)
		return redirect("/")
	return decorated_function



@app.route("/")
def home():
	return "Welcome to search"


@app.route('/v1/get_products')
@token_required
def get_products():
	query = request.args.get('q')

	# print query
	if not query:
		response_dict = {
			"status": "ERROR",
			"message": "No query set"
		}

		return jsonify(**response_dict)

	query_is_url = False
	if "http://" in query:
		query_is_url = True

	if "https://" in query:
		query_is_url = True

	if query_is_url:
		# print "in here"
		count = request.args.get('total') if request.args.get('total') else 20
		count = int(count)
		# print "got here"
		products = get_products_from_url(query, total_results=count)

		# print "products"
		if products:
			if len(products) > 0:
				response_dict = {
					"status": "OK",
					"products": products
				}
				return jsonify(**response_dict)

		response_dict = {
			"status": "ERROR",
			"message": "No enough data"
		}

		return jsonify(**response_dict)

	elif request.args.get('comparison') and request.args.get('comparison') == "yes":
		if not request.args.get('stores'):
			response_dict = {
				"status": "ERROR",
				"message": "No stores id supplied"
			}

			return jsonify(**response_dict)

		stores_id = request.args.get('stores').split(",")
		products = get_products_from_query(query, store=stores_id, comparison="yes", grouping="yes", grouping_field="store_name")

		if products:
			if len(products) > 0:
				response_dict = {
					"status": "OK",
					"products": products
				}

				return jsonify(**response_dict)

		response_dict = {
			"status": "ERROR",
			"message": "Not enough results"
		}
		return jsonify(**response_dict)
	else:
		#best guess or exact match
		#total will be 1 unless when supplied
		#best guess by default and exact match when given
		# request.args.get('best_guess') and request.args.get('best_guess') == "yes":
		best_guess = "yes"
		if request.args.get("exact_match"):
			if request.args.get('exact_match') == "yes":
				best_guess = "no"

		total = request.args.get('total') if request.args.get('total') else 1
		total = int(total)

		if request.args.get('min_score'):
			products = get_products_from_query(query, best_guess=best_guess, min_score=int(request.args.get("min_score")))
		else:
			products = get_products_from_query(query, best_guess=best_guess)


		if products:
			total_products = []
			if len(products) > 1:
				products = products[0:total]
				total_products.extend(products)
			else:
				products = products[0]
				total_products.append(products)


			response_dict = {
				"status": "OK",
				"products": total_products
			}

			return jsonify(**response_dict)

		response_dict = {
			"status": "ERROR",
			"message": "No correct match"
		}

		return jsonify(**response_dict)



	#check what products is then respond


def get_products_from_query(query, best_guess="yes", store=None, brand=None, comparison="no", grouping="no", grouping_field="", min_score=0.85):

	if not query:
		return None

	config = {
		"host": app_config["solr"]["host"],
		"port": app_config["solr"]["port"],
		"core": app_config["solr"]["core"],
	}

	client = SolrConnector(config)
	client.set_query(query)
	if store:
		client.set_filter_query("store_id", store)
	if brand:
		client.set_filter_query("brand", brand)

	if grouping == "yes":
		client.set_grouping(grouping_field, 30)

	client.set_rows(30)
	response = client.select()

	if grouping == "yes":
		#deal with grouped listes one by one here
		top_products_from_various_lists = []
		#loop through the groups and do similarity ranking for each group
		# print response["grouped"]
		if response["grouped"][grouping_field].get("groups"):

			for group in response["grouped"][grouping_field]["groups"]:
				#loop through product in each group
				# print group["doclist"]["docs"]
				products = rank_by_similarity(query, group["doclist"]["docs"], best_guess="no")
				# print products
				if len(products) > 0:
					top_products_from_various_lists.append(products[0])

			return top_products_from_various_lists
		else:
			return top_products_from_various_lists

	products = []
	if response['response'].get('docs'):
		products = response['response']['docs']

	if len(products) == 0:
		return None

	products = rank_by_similarity(query, products, best_guess=best_guess, minimum_score=min_score)

	return products


def get_products_using_clustering():
	pass

def get_products_from_url(url, total_results=20):
	if not url:
		return None

	storage_path = "/var/www/search_api/storage/"
	cache = FileSystemCache(storage_path)


	if cache.has(url):
		terms, entities_and_scores, keywords_and_scores = cache.get(url)
	else:
		terms, entities_and_scores, keywords_and_scores = get_terms_from_page(url)
		# print vals
		#then cache returned value here and expiry date is in 30 days from when it was cached
		cache.add(url, (terms, entities_and_scores, keywords_and_scores), 60*60*24*30)


	q = ""

	for term in terms:
		q +=" %s" %term

	#in case q is an empty string
	if not q:
		return None
	config = {
			"host": app_config["solr"]["host"],
			"port": app_config["solr"]["port"],
			"core": app_config["solr"]["core"],
		}

	client = SolrConnector(config)
	client.set_query(q)
	client.set_rows(total_results)

	response = client.select()

	if response is None:
		return None

	products = response['response']['docs']

	# print products

	if len(products) > 0:
		products = rank_by_terms_scores(products, entities_and_scores, keywords_and_scores)

	# print terms
	return products[0:total_results]




if __name__ == "__main__":
	# print os.path.abspath("")
	# print get_products_from_url("http://gidiurban.com/how-classy-will-we-be-without-shoes/")

	# get_products_from_query("infinix zero")
	if app_config["env"] == "development":
		app.debug = True
	# app.config['TESTING'] = True
	if app_config["env"] == "production":
		app.run(host='0.0.0.0')
	else:
		app.run()
