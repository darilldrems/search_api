import unittest
import os, sys
sys.path.insert(0, os.path.abspath(".."))

import entry

from libraries.solrconnector import SolrConnector

#test solr connector works fine
class TestSolrConnector(unittest.TestCase):
	def setUp(self):
		self.app = entry.app.test_client()

	def test_solr_response(self):
		config = {
			"host": "affiliatemonkey.co",
			"port": "8983",
			"core": "products",
		}
		
		client = SolrConnector(config)
		client.set_query("phones")
		client.set_rows(40)

		response = client.select()

		self.assertFalse(response == None)
		self.assertTrue(response['responseHeader']['status'] == 0)

	def test_token_required(self):
		rv = self.app.get('/v1/get_products', follow_redirects=True)
		# print rv.data
		assert "Welcome to search" in rv.data

	def test_queries_not_supplied(self):
		rv = self.app.get("/v1/get_products?token=hsdfgdgjk9", follow_redirects=True)

		# print rv.data
		assert "ERROR" in rv.data

	def test_best_guess(self):
		rv = self.app.get("/v1/get_products?token=hsdfgdgjk9&q=apple+iphone+6", follow_redirects=True)

		# print rv.data
		assert "status" in rv.data

	def test_best_guess(self):
		rv = self.app.get("/v1/get_products?token=hsdfgdgjk9&q=apple+iphone+6&exact_match=yes", follow_redirects=True)

		# print rv.data
		assert "status" in rv.data

	def test_best_guess_with_url(self):
		# rv = self.app.get("/v1/get_products?token=hsdfgdgjk9&q=http://www.bellanaija.com/2016/02/aim-higher-africa-forbes-africa-empowers-youngpreneurs-at-smwlagos/", follow_redirects=True)
		products = entry.get_products_from_url("http://www.bellanaija.com/2016/02/aim-higher-africa-forbes-africa-empowers-youngpreneurs-at-smwlagos/")

		# print rv.data
		assert len(products) > 0

	def test_best_guess_comparison(self):
		rv = self.app.get("/v1/get_products?token=hsdfgdgjk9&q=apple+iphone+6&comparison=yes", follow_redirects=True)

		print rv.data
		assert "status" in rv.data

if __name__ == '__main__':
    unittest.main()


