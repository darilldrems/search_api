import requests

from myconfig import app_config

def get_terms_from_page(url):
	embedly_key = app_config["embedly"]["key"]
	embedlly_url = app_config["embedly"]["url"]

	payload = {"key": embedly_key, "url": url}

	response = requests.get(embedlly_url, params=payload)

	result = response.json()

	entities = []
	keywords = []

	terms = []

	if len(result['entities']) > 0:
		entities = result['entities'][0:3]

	if len(result['keywords']) > 0:
		keywords = result['keywords'][0:3]

	
	

	for x in keywords:
		terms.append(x['name'])

	for x in entities:
		terms.append(x['name'])

	#the entities and keywords include scores
	return terms, entities, keywords