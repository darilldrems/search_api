import urllib
import requests

class SolrConnector():
	def __init__(self, config):
		self.config = config
		self.q = "*"
		self.filter_query = ""
		self.rows = "&rows=20"
		self.wt = "&wt=json"
		self.grouping = ""
	
	def set_query(self, query="*"):
		if query == "*":
			self.q = "q="+query
		else:
			#remove - and | before sending to solr
			self.q = "q="+urllib.quote_plus(query)

	def set_filter_query(self, field_or_raw, value="", operator=" OR "):
		if field_or_raw:
			if type(value) is list:
				value = operator.join(value)
				value = urllib.quote_plus(value)
				self.filter_query += "&fq=%s:(%s)" %(field_or_raw, value)

				return
			else:
				#is string
				value = urllib.quote_plus(value)
				self.filter_query += "&fq=%s:%s" %(field_or_raw, value)

				return
		
		field_or_raw = urllib.quote_plus(field_or_raw)
		self.filter_query += "&fq=%s" %field_or_raw

		return

	def set_rows(self, rows):
		self.rows = "&rows="+str(rows)
		pass

	def set_grouping(self, field, limit=10):
		self.grouping = "&group=true&group.ngroups=true&group.field="+field+"&group.limit="+str(limit)

	def select(self):
		url = "http://" + self.config['host'] + ":" + self.config['port'] + "/solr/" + self.config['core'] + "/select?"

		url += self.q + self.filter_query + self.rows + self.grouping + self.wt

		print url

		response = requests.get(url)

		if(response.status_code == 200):
			data = response.json()
			return data
		return None

if __name__ == "__main__":
	config = {
		"host": "affiliatemonkey.co",
		"port": "8983",
		"core": "products",
	}
	
	client = SolrConnector(config)
	client.set_query("phones")

	print client.select()

