
import os, sys
sys.path.insert(0, os.path.abspath(".."))

from nltk.corpus import stopwords

from libraries.words import color_names, spam_words
import re


import json
#this function uses the placcards similarity ranking algority to rerank the 
#products given and returns only the re ranked products
def rank_by_similarity(query, products, best_guess="yes", minimum_score=0.85):
	#if best guess is no then exact match is yes

	if len(products) == 0:
		return []

	if best_guess.lower() != "yes":
		exact_match = "yes"

	products_clone = products

	# print stopwords.words("english")
	# return None


	#split all products title by + and remove the right side of the +
	if best_guess == "yes": 
		for product in products_clone:
			title = product['title']
			if product.get('modified_title'):
				title = product.get('modified_title')

			titles_and_extra = title.split("+")
			product['modified_title'] = titles_and_extra[0]



	#check query if it has color

	#this will check all words in the query if there is a color
	#if no color then it will loop through all products
	#and then remove color strings from the title of all products and add it to a property color
	has_color = False
	if best_guess == "yes":
		for word in query.split():
			if word in color_names:
				has_color = True

	# print has_color

	if best_guess == "yes":
		if not has_color:
			for product in products_clone:
				product_title = product['title'].lower()

				if product.get('modified_title'):
					product_title = product['modified_title'].lower()
				# print product_title
				# return None

				for color in color_names:
					if color in product_title:
						# print "yesyesyes"
						product_title = re.sub(color, "", product_title)
						product['modified_title'] = product_title
						if product.get('_prop'):
							product['_prop']['color'] = color
						else:
							product['_prop'] = {}
							product['_prop']['color'] = color

	if best_guess == "yes":
		# print "in here"
		#remove stop words from products title
		for product in products_clone:
			product_title = product['title']
			if product.get('modified_title'):
				product_title = product.get('modified_title')

			product['modified_title'] = ' '.join([word for word in product_title.split() if word not in stopwords.words("english")])


	#loop through to remove duplicate words from title

	if best_guess == "yes":
		for product in products_clone:
			modified_title = product['title']
			if product.get('modified_title'):
				modified_title = product['modified_title']

			words = modified_title.lower().split()

			new_modified_title = modified_title.lower()
			for word in words:
				count = new_modified_title.split().count(word)
				if count > 1:
					# print new_modified_title.split()
					first_index = new_modified_title.split().index(word)
					new_string_list = []
					index = 0
					for n in new_modified_title.split():
						if n == word:
							if index != first_index:
								continue
						new_string_list.append(n)
						index = index + 1
					new_modified_title = ' '.join(new_string_list)

			product['modified_title'] = new_modified_title


	if best_guess == "yes":
		#remove spam words from product title
		for product in products_clone:
			modified_title = product['title']
			if product.get('modified_title'):
				modified_title = product['modified_title']

			modified_title = modified_title.lower()
			for word in spam_words:
				if word.lower() in modified_title:
					modified_title = re.sub(word.lower(), "", modified_title)
			product['modified_title'] = modified_title


	products_with_minimum_similarity_score = []

	#calculate similarity score
	for product in products_clone:
		title = product['title']
		if product.get('modified_title'):
			title = product.get('modified_title')

		score = similarity_score(query, title)
		# print query, " : ", title, " : ", score
		product['_similarity_score'] = score
		if score >= minimum_score:
			products_with_minimum_similarity_score.append(product)

	#sort by similarity score
	products_with_minimum_similarity_score = sorted(products_with_minimum_similarity_score, key=lambda k: k['_similarity_score'], reverse=True) 

	return products_with_minimum_similarity_score


def rank_by_terms_scores(products, entities_scores, keywords_scores):
	#this will hold the score value of each product in their respective index
	total_score_by_index = []

	for product in products:
		aggregate_score = 0
		title = product['title'].lower()
		description = ""
		if product.get('description') and len(product.get('description')) > 0:
			description = product['description'][0].lower()
		for entity in entities_scores:
			word = entity['name']
			score = entity['count']

			total_number_of_occurences_in_title = title.count(word)
			total_number_of_occurences_in_description = description.count(word)

			total = score * total_number_of_occurences_in_title * (total_number_of_occurences_in_description / 2.0)

			aggregate_score += total

		for keyword in keywords_scores:
			word = keyword['name']
			score = keyword['score']

			total_number_of_occurences_in_title = title.count(word)
			total_number_of_occurences_in_description = description.count(word)

			total = score * total_number_of_occurences_in_title * (total_number_of_occurences_in_description / 2.0)
			aggregate_score += total

		total_score_by_index.append(aggregate_score)

	if len(products) > 0:
		products_by_score = []

		for i in range(len(total_score_by_index)):
			current_max = max(total_score_by_index)
			index_of_current_max = total_score_by_index.index(current_max)
			#remove the current max from total_scrore by index in order to get the next largest
			total_score_by_index.pop(index_of_current_max)

			#add the product with the largest scrore to the products_by_score liest and return when done
			products_by_score.append(products[index_of_current_max])

			products.pop(index_of_current_max)

		return products_by_score

	return None


def similarity_score(string1, string2):
	string1 = string1.lower()
	string2 = string2.lower()

	# offset_score = __offset_score(string1, string2)
	s_score = __strikeamatch_similarty_score(string1, string2)

	# print("offsetscore"+str(offset_score))
	# print("sscore"+str(s_score))

	# if offset_score == 0:
	# 	return s_score

	# if offset_score == 1.0:
	# 	return offset_score

	# if offset_score < s_score:
	# 	return s_score

	return s_score


def __letter_pairs(word):
	num_pairs = len(word) - 1
	pairs = []
	for index in range(num_pairs):
		pair = word[index] + word[index+1]
		pairs.append(pair)

	return pairs

def __word_letter_pairs(words):
	all_words = words.split()
	all_pairs = []
	for word in  all_words:
		all_pairs.extend(__letter_pairs(word))

	return all_pairs

def __strikeamatch_similarty_score(string1, string2):
	pairs1 = set(__word_letter_pairs(string1))
	pairs2 = set(__word_letter_pairs(string2))

	union_size = len(pairs1.union(pairs2))
	
	intersection_size = len(pairs1.intersection(pairs2))

	return intersection_size/float(union_size)
	


if __name__ == "__main__":
	pass



